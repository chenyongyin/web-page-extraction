
var aForm = {
	
	pageObj: null,
	url: "",
	formId: "",
	callback: null,
	
	launch: function(url, formId, obj) {
		this.url = url;
		this.formId = formId;
		this.pageObj = obj;
		return aForm;
	},
	
	/**
	 * 初始化
	 */
	init2: function() {
		var formId = this.formId;
        var obj = this.pageObj.data;
        var pageNum = obj.pageNum;
        var startRow = obj.startRow;
        var endRow = obj.endRow;
        var pages = obj.pages;
        var total = obj.total;
        var isFirstPage = obj.isFirstPage;
        var prePage = obj.prePage;
        var isLastPage = obj.isLastPage;
        var nextPage = obj.nextPage;
        
        $(".data-table-paginate").empty();
        
        var pageInfoHtml = "<div class='table-page-info'>"
        								+ "<label><span>当前为第</span><span class='n-counts h-counts'>"+pageNum+"</span><span>页，</span></label>"
        								+ "<label><span>共</span><span class='n-counts'>"+pages+"</span><span>页</span></label>"
        								+ "</div>";
		var pagerHtml = "<div class='paginate-info'>";
		// 是第一页，将首页和上一页置灰
		if(isFirstPage) {
			pagerHtml += "<span id='first-page' class='page-button first-page page-disable'>首页</span>"
					+ "<span id='next-page' class='page-button prev-page page-disable'>上一页</span>";
		} else {
			pagerHtml += "<span id='first-page' class='page-button first-page page-hand' onclick=''>首页</span>"
					+ "<span id='next-page' class='page-button prev-page page-hand' onclick=''>上一页</span>";
		}
		pagerHtml += "<span id='dynamic-page-num'></span>";
		// 是最后一页，将末页和下一页置灰
		if(isLastPage) {
			pagerHtml += "<span id='next-page' class='page-button next-page page-disable'>下一页</span>"
					+ "<span id='last-page' class='page-button last-page page-disable'>末页</span>";
		} else {
			pagerHtml += "<span id='next-page' class='page-button next-page page-hand' onclick=''>下一页</span>"
					+ "<span id='last-page' class='page-button last-page page-hand' onclick=''>末页</span>";
		}
		pagerHtml += "</div>";
		$(".data-table-paginate").append(pageInfoHtml).append(pagerHtml);
		//aForm.settingNumbers(pageNum, pages);
		aForm.oneToSeven(pageNum, pages);
		return aForm;
	},
	/**
	 * 初始化公司和类型
	 */
	initCompanyAndType:function(obj){
		var list = obj.list;
		var len = list.length;
		if(len > 0) {
			var companyOption = '';
			var typeOption = '';
			for (i in list) {
				var typeName = '';
				if(list[i].connectType == '101'){
					typeName = 'Mysql';
				}else if(list[i].connectType == '102'){
					typeName = 'Oracle';
				}else if(list[i].connectType == '201'){
					typeName = 'CSV';
				}else{
					typeName = 'XLS';
				}
				typeOption +='<option value="' + list[i].connectType + '">' + typeName + '</option>';
				companyOption += '<option value="' + list[i].instId + '">' + list[i].instName + '</option>';
				
			}
			$("#connectTypeSelect").empty().append(typeOption);
			$("#instNameSelect").empty().append(companyOption);
		}
		
	},
	
	
	/**
	 * 初始化
	 */
	init: function() {
		var formId = this.formId;
        var obj = this.pageObj.data;
        var pageNum = obj.pageNum;
        var startRow = obj.startRow;
        var endRow = obj.endRow;
        var pages = obj.pages;
        var total = obj.total;
        var isFirstPage = obj.isFirstPage;
        var prePage = obj.prePage;
        var isLastPage = obj.isLastPage;
        var nextPage = obj.nextPage;
        //初始化公司和类型
        //aForm.initCompanyAndType(obj);
        
        $(".data-table-paginate").empty();
        //$("#totalCountId").empty().append(total);
        var pageInfoHtml = "<div class='table-page-info'>"
        								+ "<label><span>当前为第</span><span class='n-counts h-counts'>"+pageNum+"</span><span>页，</span></label>"
        								+ "<label><span>共</span><span class='n-counts'>"+pages+"</span><span>页</span></label>"
        								+ "</div>";
		var pagerHtml = "<div class='paginate-info'>"
					+'<span id="first-page" class="page-button first-page page-hand" onclick="aForm.formPaging(1)">'
					+'首页'
					+'</span>';
		if(!isFirstPage){ // 如果是第一页，则只显示下一页、尾页
			pagerHtml += '<span id="previous-page" class="page-button prev-page page-hand" onclick="aForm.formPaging(\'' + prePage + '\')">'
                    +'上一页'
                    +'</span>';
        }
		pagerHtml += "<span id='dynamic-page-num'></span>";
		if(!isLastPage){ // 如果是末页，则只显示上一页
			pagerHtml += '<span id="next-page" class="page-button next-page page-hand"   onclick="aForm.formPaging(\'' + nextPage + '\')">'
                                +'下一页'
                        +'</span>'
                        +'<span id="last-page" class="page-button last-page page-hand" onclick="aForm.formPaging(\'' + pages +'\')" >'
                            +'末页'
                        +'</span>';
        }
		pagerHtml += "</div>";
		$(".data-table-paginate").append(pageInfoHtml).append(pagerHtml);
		//aForm.settingNumbers(pageNum, pages);
		aForm.oneToSeven(pageNum, pages);
		return aForm;
	},
	
	
    /**
     * 上一页与下一页中间的7个数字的切换与颜色变换
     * @param pageNum
     * @param pages
     */
    oneToSeven : function(pageNum , pages ){
        var curpage = parseInt(pageNum);
        var pclassAc = 'page-button page-pager-active';       // 当前按钮样式类
        var pclassBt = 'page-button page-pager-hand';       // 非当前样式类
        var pageCount =  parseInt(pages) ;
        var html_ = '';
        if(pageCount > 0 && pageCount < 8){
            for(var i =1 ; i < pageCount+1 ; i++){
                if(curpage == i){
                    html_ += '<span class="' + pclassAc + '">' + i + '</span>';
                }else{
                    html_ += '<span class="' + pclassBt + '"  onclick="aForm.formPaging(\'' + i +'\')" >' + i + '</span>';
                }
            }
        }else if(pageCount > 7){
            if(curpage < 5){
                for(var i =1 ; i < 8 ; i++){
                    if(curpage == i){
                        html_ += '<span class="' + pclassAc + '">' + i + '</span>';
                    }else{
                        html_ += '<span class="' + pclassBt + '"  onclick="aForm.formPaging(\'' + i +'\')" >' + i + '</span>';
                    }
                }
            }else{
                var arr = new Array();
                if((pageCount - curpage) < 7 ){ // 最后7页
                    for(var i = 0 ; i < 7 ; i ++){
                        arr[i] = pageCount - 6 + i;
                    }
                }else{
                    arr[0] = curpage - 3;
                    arr[1] = curpage - 2;
                    arr[2] = curpage - 1;
                    arr[3] = curpage;
                    arr[4] = curpage + 1;
                    arr[5] = curpage + 2;
                    arr[6] = curpage + 3;
                }
                for(var i = 0 ; i < arr.length ; i ++){
                    if(curpage == arr[i]){
                        html_ += '<span class="' + pclassAc + '">' + arr[i] + '</span>';
                    }else{
                        html_ += '<span class="' + pclassBt + '"  onclick="aForm.formPaging(\'' + arr[i] +'\')" >' + arr[i] + '</span>';
                    }
                }
            }

        }
        $("#dynamic-page-num").append(html_);
    },
	
	/**
	 * 数显分页
	 * @param pageNum
	 * @param pages
	 */
	settingNumbers: function(pageNum, pages){
		var _activeNum = "page-pager-active";
		var _normalNum = "page-pager-hand";
		var curPage = parseInt(pageNum);
		var pageCount = parseInt(pages);
		var content = "";
		if(pageCount <= 7) {
			// 处理pageCount不正常时，仅有1页
			pageCount = pageCount <= 0 ? 1 : pageCount;
			for(var i = 1; i <=pageCount; i ++) {
				if(curPage == i) {
					content += "<span class='page-button "+_activeNum+"'>" + i + "</span>";
				} else {
					content += "<span class='page-button "+_normalNum+"' onclick='aForm.formPaging( "+i+" )'>" + i + "</span>";
				}
			}
		} else {
			if(curPage < 5) {
				for(var i = 1; i <=7; i ++) {
					if(curPage == i) {
						content += "<span class='page-button "+_activeNum+"'>" + i + "</span>";
					} else {
						content += "<span class='page-button "+_normalNum+"' onclick='aForm.formPaging( "+i+" )'>" + i + "</span>";
					}
				}
			} else {
				var arr = new Array();
				 // 最后7页
                if( (pageCount - curPage) < 7 ){
                    for(var i = 0 ; i < 7 ; i ++){
                        arr[i] = pageCount - 6 + i;
                    }
                } else {
                    arr[0] = curPage - 3;
                    arr[1] = curPage - 2;
                    arr[2] = curPage - 1;
                    arr[3] = curPage;
                    arr[4] = curPage + 1;
                    arr[5] = curPage + 2;
                    arr[6] = curPage + 3;
                }
                for(var i = 0; i < arr.length; i ++) {
                	var num = arr[i];
					if(curPage == num) {
						content += "<span class='page-button "+_activeNum+"'>" + num + "</span>";
					} else {
						content += "<span class='page-button "+_normalNum+"' onclick='aForm.formPaging( "+num+" )'>" + num + "</span>";
					}
				}
			}
			$("#dynamic-page-num").append(content);
		}
	},
	
	/**
     * 触发分页事件
     * @param pageNum
     */
    formPaging : function(pageNum){
        var parse = aForm.parseUrl(this.url);
        var url = parse.protocol + '://' + parse.host + ':' + parse.port + parse.path;
       var pageSize = 10;
        var actions = url + '?pageNum=' + pageNum +'&pageSize=' + pageSize;
        if(this.callName != null && (typeof this.callName=="function")){
            this.callName(actions);
        }
    },

    /**
     * 绘制表单
     * @param callback函数
     */
    drawForm : function(callback){
        callback();
        eval("this.callName = callback;");
        return aForm;
    },
    
    /**
     * 地址解析
     * @param url
     */
    parseUrl : function(url) {
        var a =  document.createElement('a');
        a.href = url;
        return {
            source: url,
            protocol: a.protocol.replace(':',''),
            host: a.hostname,
            port: a.port,
            query: a.search,
            params: (function(){
                var ret = {},
                    seg = a.search.replace(/^\?/,'').split('&'),
                    len = seg.length, i = 0, s;
                for (;i<len;i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),
            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
            hash: a.hash.replace('#',''),
            path: a.pathname.replace(/^([^\/])/,'/$1'),
            relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
            segments: a.pathname.replace(/^\//,'').split('/')
        };
    }
};
