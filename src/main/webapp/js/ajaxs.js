
var ajaxs = {
	 sendAjax:function(type_, url_ , data_){
		   /* if(parent.localStorage.token!=undefined){
		    	var token = parent.localStorage.token ? parent.localStorage.token : "";
		    	url_=url_.indexOf("jsid")>0?url_:(url_.indexOf("?")>0?(url_+"&jsid="+token):(url_+"?jsid="+token));
		    }*/
	        var msg_ = null;
	        $.ajax({
	            dataType : "text",
	            type :type_,
	            url : url_,
	            data : data_,
	            async : false,
	            success : function(msg) {
	                msg_ = msg;
	            },
	            error: function(msg) {
	                msg_ = msg;
	            }
	        });
	        return msg_;
	 },
	asyncAjax: function(_type, _url, _dtype, _data) {
		if(parent.localStorage.token!=undefined){
	    	var token = parent.localStorage.token ? parent.localStorage.token : "";
	    	url_=url_.indexOf("jsid")>0?url_:(url_.indexOf("?")>0?(url_+"&jsid="+token):(url_+"?jsid="+token));
	    }
		var result = null;
		$.ajax({
			type: _type,
			url: _url,
			data: _data,
			dataType: _dtype,
			async: true,
			success: function(_result) {
				result = _result;
			},
			error: function(_result) {
				result = _result;
			}
		});
		return result;
	}
};