<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="Cache-Control" content="no-siteapp"/><meta name="renderer" content="webkit">
    <meta name="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="/image/favicon.png" />
    <title>网页提取</title>
    <link rel="stylesheet" href="/css/login.css"/></head>
<body>
<div class="index-bk index-bk-cg" style="padding-top: 20%;text-align: center;">
    <div class="form-line" style="text-align: center;height:86px;">
        <label class="input-line stack-on" style="width: 21%;margin-left: -15%;">
            <input id="url-input" class="font-minus" placeholder="请输入网址" style="width:100%;" type="text" id="captcha" name="captcha" tabindex="3" autocomplete="off" value=""></label>
        <label id="very_block" class="very-block">
            <label id="download-btn" class="login-button public-font have-point" style=" width: 10%; margin-left: -35%;">提取</label>
        </label>
    </div>
</div>
</body>
<script src="/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="/js/ajax-form.js"></script>
<script type="text/javascript" src="/js/ajaxs.js"></script>
<script type="text/javascript">
    $("#download-btn").click(function(event){
        window.location = '${basePath}/download?url='+$("#url-input").val();
    });
</script>
</html>