package cyy.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: JspExports
 * @ProjectName web-page-extraction
 * @Description: TODO
 * @date 2019/12/2 15:17
 */
public class JspExportUtil {
    /**
     * 创建文件
     *
     * @throws IOException
     */
    public static String creatTxtFile(String path, String name) throws IOException {
        boolean flag = false;
        File filename1 = new File(path);
        if (!filename1.exists()) {
            filename1.mkdirs();
        }
        String filenameTemp = path + "/" + name + ".html";
        File filename = new File(filenameTemp);
        if (!filename.exists()) {
            filename.createNewFile();
        }
        return filenameTemp;
    }

    /**
     * 写文件
     *
     * @param newStr 新内容
     * @throws IOException
     */
    public static boolean writeTxtFile(String filenameTemp, String newStr) throws IOException {
        // 先读取原有文件内容，然后进行写入操作
        boolean flag = false;
        String filein = newStr + "\r\n";
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            // 文件路径
            File file = new File(filenameTemp);
            // 将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buf = new StringBuffer();

            // 保存该文件原有的内容
            for (int j = 1; (temp = br.readLine()) != null; j++) {
                buf = buf.append(temp);
                // System.getProperty("line.separator")
                // 行与行之间的分隔符 相当于“\n”
                buf = buf.append(System.getProperty("line.separator"));
            }
            buf.append(filein);

            fos = new FileOutputStream(file);
            pw = new PrintWriter(new OutputStreamWriter(fos, "utf-8"), true);

            pw.write(buf.toString().toCharArray());
            pw.flush();
            flag = true;
        } catch (IOException e1) {
            // TODO 自动生成 catch 块
            throw e1;
        } finally {
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return flag;
    }

    public static String regx(String html, String path, String regx, String head, String url, String folderName) {
        //下载某种资源文件
        //新建一个正则表达式
        Pattern pattern = Pattern.compile(regx);
        //对网页源代码进行查找匹配
        Matcher matcher = pattern.matcher(html);
        //对符合条件的结果逐条做处理
        while (matcher.find()) {
            String matcherGroup = matcher.group().replaceAll("'", "\"");
            System.out.println(matcherGroup);
            Matcher matcherNew = Pattern.compile(head + ".*\"").matcher(matcherGroup);
            if (matcherNew.find()) {

                //对于CSS匹配，查找出的结果形如：href="skins/default/css/base.css" rel="stylesheet" type="text/css"
                String myUrl = matcherNew.group();
                //去掉前面的头部，如：href:"
                myUrl = myUrl.replaceAll(head, "");
                //从第一个引号开始截取真正的内容，如：skins/default/css/base.css
                myUrl = myUrl.substring(0, myUrl.indexOf("\""));
                //获取样式表文件的文件名，如：base.css
                String myName = getUrlFileName(myUrl);
                //替换html文件中的资源文件
                html = html.replaceAll(myUrl, folderName + "/" + myName);
                //得到最终的资源文件URL，如：http://www.hua.com/skins/default/css/base.css
                myUrl = joinUrlPath(url, myUrl);
                //System.out.println("发生地健康："+myUrl);
                //去掉文件名不合法的情况，不合法的文件名字符还有好几个，这里只随便举例几个
                if (!myName.contains("\"") && !myName.contains("/")) {
                    if (myName.contains("?")) {
                        myName = myName.split("\\?")[0];
                    }
                    String newfilePath = path + "/" + folderName + "/" + myName;
                    //开始下载文件
                    downloadFile(myUrl, newfilePath);
                    System.out.println("成功下载文件：" + myName);
                    //如果是下载css文件
                    if (regx.contains("text/css")) {
                        //将刚刚下载的CSS文件实例化
                        File cssFile = new File(path + "/" + folderName + "/" + myName);
                        //读取CSS文件的内容，这里用默认的gb2312编码
                        String txt = readFile(cssFile, "utf-8");
                        //开始匹配背景图片
                        Matcher matcherUrl = Pattern.compile("background:.*url\\(.*\\)").matcher(txt);
                        while (matcherUrl.find()) {
                            //去掉前面和后面的标记,得到的结果如：../images/ico_4.gif
                            String temp = matcherUrl.group().replaceAll(" ", "").replaceAll("background:url\\(", "").replaceAll("\\)", "");
                            //拼接出真正的图片路径，如：http://www.hua.com/skins/default/images/ico_4.gif
                            String backgroundUrl = joinUrlPath(myUrl, temp);
                            String prevPath = myUrl.split("css")[0];
                            if (temp.contains("..")) {
                                int prevLength = temp.split("\\.\\.").length;
                                if (prevLength == 2) {
                                    backgroundUrl = prevPath + temp.replaceAll("\\.\\./", "");
                                }
                            }
                            //获取背景图片的文件名，如：ico_4.gif
                            String backgroundFileName = getUrlFileName(backgroundUrl);
                            //背景图片要保存的路径，如：c:/users\lxa\desktop\网页\images\ico_4.gif
                            String backgroundFilePath = path + "/img/" + backgroundFileName;
                            //替换html文件中的资源文件
                            html = html.replaceAll(temp, "../img/" + backgroundFilePath);
                            //如果不存在同名文件
                            if (!new File(backgroundFilePath).exists()) {
                                //开始下载背景图片
                                if (downloadFile(backgroundUrl, backgroundFilePath)) {
                                    System.out.println("成功下载背景图片：" + backgroundFileName);
                                }
                            } else {
                                System.out.println("指定文件夹已存在同名文件，已为您自动跳过：" + backgroundFilePath);
                            }
                        }
                    }

                }
            }
        }
        return html;
    }

    /**
     * 获取URL中最后面的真实文件名
     *
     * @param url 如：http://www.hua.com/bg.jpg
     * @return 返回bg.jpg
     */
    public static String getUrlFileName(String url) {
        return url.split("/")[url.split("/").length - 1];
    }

    public static String joinUrlPath(String url, String fileName) {
        //System.out.println("url:"+url);
        //System.out.println("fileName:"+fileName);
        if (fileName.startsWith("http://")) {
            return fileName;
        }
        //如果去掉“http://”前缀后还包含“/”符，说明要退一层目录，即去掉当前文件名
        if (url.startsWith("http://") && url.replaceAll("http://", "").contains("/")) {
            url = getUrlPath(url);
        } else if (url.startsWith("https://") && url.replaceAll("https://", "").contains("/")) {
            url = getUrlPath(url);
        }
        if (fileName.startsWith("../") || fileName.startsWith("/")) {

            if (url.startsWith("http://")) {
                while (url.replaceAll("http://", "").contains("/")) {
                    url = getUrlPath(url);
                }
            } else if (url.startsWith("https://")) {
                while (url.replaceAll("https://", "").contains("/")) {
                    url = getUrlPath(url);
                }
            }
            //只有当前URL包含多层目录才能后退，如果只是http://www.hua.com，想后退都不行
            fileName = fileName.substring(fileName.indexOf("/") + 1);
        }
        if (fileName.startsWith("http")) {
            return fileName;
        } else {
            return url + "/" + fileName;
        }

    }

    /**
     * 获取URL不带文件名的路径
     *
     * @param url 如：http://www.hua.com/bg.jpg
     * @return 返回 http://www.hua.com
     */
    public static String getUrlPath(String url) {
        return url.replaceAll("/" + getUrlFileName(url), "");
    }


    /**
     * 根据URL下载某个文件
     *
     * @param fileURL  下载地址
     * @param filePath 存放的路径
     */
    public static boolean downloadFile(String fileURL, String filePath) {
        try {
            File file = new File(filePath);
            file.createNewFile();
            StringBuffer sb = new StringBuffer();
            URL url = new URL(fileURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //设置客户的浏览器为IE9
            connection.setRequestProperty("User-Agent", "MSIE 9.0");
            byte[] buffer = new byte[1024];
            InputStream is = connection.getInputStream();
            FileOutputStream fos = new FileOutputStream(file);
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            is.close();
            connection.disconnect();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("该文件不存在：" + fileURL);
            return false;
        }

    }

    /**
     * 读取某个文本文件的内容
     *
     * @param file   要读取的文件
     * @param encode 读取文件的编码方式
     * @return 返回读取到的内容
     */
    public static String readFile(File file, String encode) {
        try {
            InputStreamReader read = new InputStreamReader(new FileInputStream(file), encode);
            BufferedReader bufread = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String str = "";
            while ((str = bufread.readLine()) != null) {
                sb.append(str + "\n");
            }
            String txt = new String(sb);
            return txt;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
