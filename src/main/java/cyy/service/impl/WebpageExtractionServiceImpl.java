package cyy.service.impl;

import cyy.service.IWebpageExtractionService;
import cyy.utils.DownloadFileUtil;
import cyy.utils.JspExportUtil;
import cyy.utils.ZipUtils;
import net.iwanglu.HttpGetRequest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: WebpageExtractionServiceImpl
 * @ProjectName web-page-extraction
 * @Description: TODO
 * @date 2019/12/2 15:08
 */
@Service
public class WebpageExtractionServiceImpl implements IWebpageExtractionService {
    @Override
    public Object downloadJsp(HttpServletRequest httpServletRequest, String url) throws IOException {
        String path = httpServletRequest.getServletContext().getRealPath("/");
        if(url.endsWith("/")){
            url = url.substring(0,url.length()-1);
        }
//        String path = "D:/webpage/";
                Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String folderName = sdf.format(d);
        String basePath = path + folderName + "/";
        String cssPath = basePath + "css/";
        String jsPath = basePath + "js/";
        String imgPath = basePath + "img/";
        File jsFile = new File(jsPath);
        File cssFile = new File(cssPath);
        File imgFile = new File(imgPath);
        if (!jsFile.exists()) {
            jsFile.mkdirs();
        }
        if (!cssFile.exists()) {
            cssFile.mkdirs();
        }
        if (!imgFile.exists()) {
            imgFile.mkdirs();
        }
        HttpGetRequest httpGetRequest = new HttpGetRequest();
        String html = httpGetRequest.getResponseJson(url);
        // 替换js
        html = JspExportUtil.regx(html,basePath, "<script[^>]*?>.*?</script>", "src=\"", url, "js");
        // 替换图片
        html = JspExportUtil.regx(html,basePath, "<\\s*img\\s+([^>]+)\\s*>", "src=\"", url, "img");
        // 替换css
        html = JspExportUtil.regx(html,basePath, "<*type=\"text/css\"\\s+([^>]+)\\s*>", "href=\"", url, "css");
        String filenameTemp = JspExportUtil.creatTxtFile(basePath, "index");
        JspExportUtil.writeTxtFile(filenameTemp,html);
        FileOutputStream fos1 = new FileOutputStream(new File(path+folderName+".zip"));
        ZipUtils.toZip(path+folderName, fos1, true);
        FileSystemResource fileSource = new FileSystemResource(new File(path+folderName+".zip"));
        return DownloadFileUtil.downloadFile(fileSource.getInputStream(), fileSource.getFilename());
    }
}
