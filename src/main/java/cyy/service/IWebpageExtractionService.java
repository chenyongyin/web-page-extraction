package cyy.service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: IWebpageExtractionService
 * @ProjectName web-page-extraction
 * @Description: TODO
 * @date 2019/12/2 15:07
 */
public interface IWebpageExtractionService {

    Object downloadJsp(HttpServletRequest httpServletRequest,String path) throws IOException;

}
