package cyy.controller;

import cyy.service.IWebpageExtractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: downloadJspFileController
 * @ProjectName web-page-extraction
 * @Description:
 * @date 2019/12/3 15:35
 */
@Controller
@RequestMapping("/download")
public class DownloadJspFileController {
    @Autowired
    private IWebpageExtractionService webpageExtractionService;
    @RequestMapping(value = "")
    @ResponseBody
    public Object createJob(HttpServletRequest httpServletRequest,String url) throws Exception{
        httpServletRequest.getContextPath();
        return webpageExtractionService.downloadJsp(httpServletRequest,url);
    }
}
